import * as React from 'react';
import '../css/styles.css';
import * as PropTypes from 'prop-types';
interface Props {
    /**
     * Whether to make this notification accessible from anywhere.
     */
    shared?: boolean;
}
declare class State {
    message: string;
    visible: boolean;
}
/**
 * Displays a small, important message.
 * @author Johan Svensson
 */
export default class Notification extends React.Component<Props, State> {
    state: State;
    static propTypes: {
        shared: PropTypes.Requireable<boolean>;
    };
    /**
     * @returns All shared and mounted notification instances.
     */
    static readonly shared: Notification[];
    /**
     * Shows a message with the first shared component, if one exists.
     */
    static showWithFirstShared(message: string): void;
    animationDuration: number;
    hideTimeoutId: number;
    componentDidMount(): void;
    componentWillUnmount(): void;
    /**
     * Shows a notification message.
     */
    show(message: string, duration?: number): void;
    hide(): void;
    render(): JSX.Element;
}
export {};
